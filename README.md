# Course Catalogue Setup

The search index offers the possibility to search quickly for various university courses.

With this project you can set up all components that are necessary to run the index. The process uses [disresi-setup](https://gitlab.com/oersi/disresi/disresi-setup) to install the components.

This repo contains the default config adjustments for the course-catalogue-search-index. For testing purposes, you can use this repo to set up a local installation in a local VirtualBox VM via Vagrant on your own computer. But you can also use this repo as base for your production installation of a course-catalogue-instance via GitLab-CI or Direct Ansible-Installation. For this you need to enrich the default configuration by instance-specific values like passwords, usernames etc. - just follow the search-index documentation.

Detailed documentation can be found in the search-index documentation.

## Quick start - local testing

Prerequisites
* [Git](https://git-scm.com/downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Perform the following steps in the terminal (Linux / macOS) or in the GitBash (Windows).

```
git clone https://gitlab.com/TIBHannover/course-catalogue/course-catalogue-setup.git
cd course-catalogue-setup
vagrant up
```

When the installation is complete (a few minutes, depending on the download speed), the index can be opened in the browser

For local Vagrant VirtualBox:

<http://192.168.98.116/>

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
